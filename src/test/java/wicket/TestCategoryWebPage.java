package wicket;

import com.example.wicket.CategoryWebPage;
import com.example.wicket.form.CategoryForm;
import com.example.wicket.navigation.TaskNavigation;
import org.junit.Before;
import org.junit.Test;

public class TestCategoryWebPage extends TestConfiguration {

    @Before
    public void init() {
        wicketTester.startPage(CategoryWebPage.class);
    }

    @Test
    public void testCategoryWebPageRendersSuccessful() {
        wicketTester.assertRenderedPage(CategoryWebPage.class);
    }

    @Test
    public void testCategoryWebPageContainsNavigation() {
        wicketTester.assertComponent("navigation", TaskNavigation.class);
    }

    @Test
    public void testCategoryWebPageContainsCategoryForm() {
        wicketTester.assertComponent("categoryForm", CategoryForm.class);
    }

}
