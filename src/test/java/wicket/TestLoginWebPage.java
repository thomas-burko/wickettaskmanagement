package wicket;

import com.example.configuration.SecurityConfiguration;
import com.example.wicket.LoginWebPage;
import com.example.wicket.form.LoginForm;
import com.example.wicket.navigation.TaskNavigation;
import org.junit.Before;
import org.junit.Test;

public class TestLoginWebPage extends TestConfiguration {

    @Before
    public void init() {
        SecurityConfiguration.get().signOut();
        wicketTester.startPage(LoginWebPage.class);
    }

    @Test
    public void testLoginWebPageRendersSuccessful() {
        wicketTester.assertRenderedPage(LoginWebPage.class);
    }

    @Test
    public void testLoginWebPageContainsNavigation() {
        wicketTester.assertComponent("navigation", TaskNavigation.class);
    }

    @Test
    public void testLoginWebPageContainsLoginForm() {
        wicketTester.assertComponent("loginForm", LoginForm.class);
    }

}
