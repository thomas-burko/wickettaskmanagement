package wicket;

import com.example.wicket.TaskListWebPage;
import com.example.wicket.form.RemoveTaskForm;
import com.example.wicket.listView.TaskListView;
import com.example.wicket.navigation.TaskNavigation;
import org.junit.Before;
import org.junit.Test;

public class TestTaskListWebPage extends TestConfiguration {

    @Before
    public void init() {
        wicketTester.startPage(TaskListWebPage.class);
    }

    @Test
    public void testTaskListWebPageRendersSuccessful() {
        wicketTester.assertRenderedPage(TaskListWebPage.class);
    }


    @Test
    public void testTaskListWebPageContainsNavigation() {
        wicketTester.assertComponent("navigation", TaskNavigation.class);
    }

    @Test
    public void testTaskListWebPageContainsTaskListView() {
        wicketTester.assertComponent("tasks", TaskListView.class);
    }

    @Test
    public void testTaskListWebpageContainsRemoveTaskForm() {
        wicketTester.assertComponent("removeTask", RemoveTaskForm.class);
    }

}
