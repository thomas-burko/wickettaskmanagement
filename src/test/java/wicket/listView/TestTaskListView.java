package wicket.listView;

import com.example.domain.Task;
import com.example.wicket.TaskListWebPage;
import org.junit.Before;
import org.junit.Test;
import wicket.TestConfiguration;

import java.util.ArrayList;
import java.util.List;

public class TestTaskListView extends TestConfiguration {


    @Before
    public void init() {
        wicketTester.startPage(TaskListWebPage.class);
    }

    @Test
    public void testTaskListView() {
        List<Task> tasks = new ArrayList<Task>();
        wicketTester.assertListView("tasks", tasks);
    }
}
