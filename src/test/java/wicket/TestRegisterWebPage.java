package wicket;

import com.example.wicket.RegisterWebPage;
import com.example.wicket.form.RegisterForm;
import com.example.wicket.navigation.TaskNavigation;
import org.junit.Before;
import org.junit.Test;

public class TestRegisterWebPage extends TestConfiguration {

    @Before
    public void init() {
        wicketTester.startPage(RegisterWebPage.class);
    }

    @Test
    public void testRegisterWebPageRendersSuccessful() {
        wicketTester.assertRenderedPage(RegisterWebPage.class);
    }

    @Test
    public void testRegisterWebPageContainsNavigation() {
        wicketTester.assertComponent("navigation", TaskNavigation.class);
    }

    @Test
    public void testRegisterWebPageContainsRegisterForm() {
        wicketTester.assertComponent("registerForm", RegisterForm.class);
    }

}
