package wicket;

import com.example.WicketApplication;
import com.example.configuration.SecurityConfiguration;
import com.example.configuration.SpringConfiguration;
import com.example.exceptions.UserAlreadyExistingException;
import com.example.service.CategoryService;
import com.example.service.UserService;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Test configuration class for Wicket unit tests.
 *
 * @author Thomas Burko
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = SpringConfiguration.class)
@Transactional
@Ignore
public class TestConfiguration {

    @Autowired
    protected UserService userService;

    @Autowired
    protected CategoryService categoryService;

    @Autowired
    private WicketApplication wicketApplication;

    protected WicketTester wicketTester;

    /**
     * Set up the WicketTester with WicketApplication and a signed in User
     */
    @Before
    public void setUpTestSuite() throws UserAlreadyExistingException {
        wicketTester = new WicketTester(wicketApplication);
        userService.save("username", "password");
        SecurityConfiguration.get().signIn("username", "password");
    }

}
