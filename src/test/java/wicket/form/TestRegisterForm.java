package wicket.form;

import com.example.configuration.SecurityConfiguration;
import com.example.wicket.TaskWebPage;
import com.example.wicket.RegisterWebPage;
import org.apache.wicket.util.tester.FormTester;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import wicket.TestConfiguration;

import static org.junit.Assert.assertTrue;

public class TestRegisterForm extends TestConfiguration {

    private FormTester formTester;

    @Before
    public void init() {
        wicketTester.startPage(RegisterWebPage.class);
        formTester = wicketTester.newFormTester("registerForm");
        SecurityConfiguration.get().signOut();
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRegisterFormRendersSuccessful() {
        wicketTester.assertRenderedPage(RegisterWebPage.class);
    }

    @Test
    public void testRegisterFormInvalidUsernameMinimumLength() {
        formTester.setValue("username", "ab");
        formTester.setValue("password", "musterpasswort");
        formTester.submit();
        wicketTester.assertErrorMessages("Der Wert von 'username' muss mindestens 3 Zeichen lang sein.");
    }

    @Test
    public void testRegisterFormInvalidPasswordMinimumLength() {
        formTester.setValue("username", "Max Mustermann");
        formTester.setValue("password", "ab");
        formTester.submit();
        wicketTester.assertErrorMessages("Der Wert von 'password' muss mindestens 3 Zeichen lang sein.");
    }

    @Test
    public void testRegisterFormRegisterSuccessful() {
        formTester.setValue("username", "newUser");
        formTester.setValue("password", "password");
        formTester.submit();
        wicketTester.assertNoErrorMessage();
        assertTrue(SecurityConfiguration.get().isSignedIn());
        wicketTester.assertRenderedPage(TaskWebPage.class);
    }

}
