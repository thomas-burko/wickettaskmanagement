package wicket.form;

import com.example.configuration.SecurityConfiguration;
import com.example.domain.Category;
import com.example.wicket.TaskWebPage;
import com.example.wicket.TaskListWebPage;
import org.apache.wicket.util.tester.FormTester;
import org.junit.Before;
import org.junit.Test;
import wicket.TestConfiguration;

public class TestTaskForm extends TestConfiguration {

    private FormTester formTester;

    @Before
    public void init() {
        categoryService.save(new Category("Test"), SecurityConfiguration.getUser().getReference());
        wicketTester.startPage(TaskWebPage.class);
        formTester = wicketTester.newFormTester("taskForm");
    }

    @Test
    public void testTaskFormNoCategorySelected() {
        formTester.setValue("text", "test");
        formTester.setValue("date", "01.01.2016");
        formTester.select("priority", 1);
        formTester.submit();
        wicketTester.assertErrorMessages("Bitte waehlen Sie eine Kategorie!");
        wicketTester.assertRenderedPage(TaskWebPage.class);
    }

    @Test
    public void testTaskFormTextSmallerThree() {
        formTester.select("category", 0);
        formTester.setValue("text", "ab");
        formTester.setValue("date", "01.01.2016");
        formTester.select("priority", 1);
        formTester.submit();
        wicketTester.assertErrorMessages("Der Wert von 'text' muss mindestens 3 Zeichen lang sein.");
        wicketTester.assertRenderedPage(TaskWebPage.class);
    }

    @Test
    public void testTaskFormNoDate() {
        formTester.select("category", 0);
        formTester.setValue("text", "test");
        formTester.select("priority", 1);
        formTester.submit();
        wicketTester.assertErrorMessages("Bitte tragen Sie einen Wert im Feld 'date' ein.");
        wicketTester.assertRenderedPage(TaskWebPage.class);
    }

    @Test
    public void testTaskFormNoPrioritySelected() {
        formTester.select("category", 0);
        formTester.setValue("text", "test");
        formTester.setValue("date", "01.01.2016");
        formTester.submit();
        wicketTester.assertErrorMessages("Bitte waehlen Sie eine Prioritaet!");
        wicketTester.assertRenderedPage(TaskWebPage.class);
    }

    @Test
    public void testTaskFormValidInput() {
        formTester.select("category", 0);
        formTester.setValue("text", "test");
        formTester.setValue("date", "01.01.2016");
        formTester.select("priority", 1);
        formTester.submit();
        wicketTester.assertNoErrorMessage();
        wicketTester.assertRenderedPage(TaskListWebPage.class);
    }
}
