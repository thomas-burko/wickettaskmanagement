package wicket.form;

import com.example.configuration.SecurityConfiguration;
import com.example.exceptions.UserAlreadyExistingException;
import com.example.wicket.LoginWebPage;
import com.example.wicket.TaskWebPage;
import org.apache.wicket.util.tester.FormTester;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import wicket.TestConfiguration;

public class TestLoginForm extends TestConfiguration {

    private FormTester formTester;

    @Before
    public void init() {
        SecurityConfiguration.get().signOut();
        wicketTester.startPage(LoginWebPage.class);
        formTester = wicketTester.newFormTester("loginForm");
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testLoginFormRendersSuccessful() {
        wicketTester.assertRenderedPage(LoginWebPage.class);
    }

    @Test
    public void testLoginFormSuccessfulLogin() throws UserAlreadyExistingException {
        formTester.setValue("username", "username");
        formTester.setValue("password", "password");
        formTester.submit();
        wicketTester.assertNoErrorMessage();
        wicketTester.assertRenderedPage(TaskWebPage.class);
    }

    @Test
    public void testLoginFormUnsuccessfulLogin() {
        formTester.setValue("username", "wrongUser");
        formTester.setValue("password", "wrongPassword");
        formTester.submit();
        wicketTester.assertRenderedPage(LoginWebPage.class);
    }

    @Test
    public void testLoginFormUnsuccessfulLoginWrongPassword() {
        formTester.setValue("username", "username");
        formTester.setValue("password", "wrongPassword");
        formTester.submit();
        wicketTester.assertRenderedPage(LoginWebPage.class);
    }

}
