package wicket.form;

import com.example.domain.Category;
import com.example.domain.Priority;
import com.example.domain.Task;
import com.example.wicket.TaskListWebPage;
import org.apache.wicket.util.tester.FormTester;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import wicket.TestConfiguration;

public class TestRemoveTaskForm extends TestConfiguration {

    private FormTester formTester;

    @Before
    public void init() {
        wicketTester.startPage(TaskListWebPage.class);
        formTester = wicketTester.newFormTester("removeTask");
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testRemoveTaskFormRendersSucessful() {
        wicketTester.assertRenderedPage(TaskListWebPage.class);
    }

    @Test
    public void testRemoveTaskFormRedirectToTaskListWebPage() {
        Task t = new Task(new Category("Test"), "test", Priority.Minor.toString());
        t.setFinished(true);
        formTester.submit();
        wicketTester.assertRenderedPage(TaskListWebPage.class);
    }

}
