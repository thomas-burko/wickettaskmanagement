package wicket.form;

import com.example.wicket.CategoryWebPage;
import org.apache.wicket.util.tester.FormTester;
import org.junit.Before;
import org.junit.Test;
import wicket.TestConfiguration;

public class TestCategoryForm extends TestConfiguration {

    private FormTester formTester;

    @Before
    public void init() {
        wicketTester.startPage(CategoryWebPage.class);
        formTester = wicketTester.newFormTester("categoryForm");
    }

    @Test
    public void testCategoryFormRendersSuccessful() {
        wicketTester.assertRenderedPage(CategoryWebPage.class);
    }

    @Test
    public void testCategoryFormAddCategorySuccessful() {
        formTester.setValue("categoryName", "categoryName");
        formTester.submit();
        wicketTester.assertNoErrorMessage();
    }

    @Test
    public void testCategoryFormAddCategoryUnsuccessful() {
        formTester.submit();
        wicketTester.assertErrorMessages("Bitte tragen Sie einen Wert im Feld 'categoryName' ein.");
    }

}
