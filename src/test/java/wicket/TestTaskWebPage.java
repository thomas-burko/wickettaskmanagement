package wicket;

import com.example.wicket.TaskWebPage;
import com.example.wicket.form.TaskForm;
import com.example.wicket.navigation.TaskNavigation;
import org.junit.Before;
import org.junit.Test;

public class TestTaskWebPage extends TestConfiguration {

    @Before
    public void init() {
        wicketTester.startPage(com.example.wicket.TaskWebPage.class);
    }

    @Test
    public void testNewTaskWebPageRendersSuccessful() {
        wicketTester.assertRenderedPage(com.example.wicket.TaskWebPage.class);
    }

    @Test
    public void testNewTaskWebPageContainsNavigation() {
        wicketTester.assertComponent("navigation", TaskNavigation.class);
    }

    @Test
    public void testNewTaskWebPageContainsTaskForm() {
        wicketTester.assertComponent("taskForm", TaskForm.class);
    }

    @Test
    public void testNewTaskWebPageContainsLabelWithCurrentUser() {
        wicketTester.assertLabel("currentUser", "username");
    }

}
