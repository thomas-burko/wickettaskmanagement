package wicket;

import com.example.configuration.SecurityConfiguration;
import com.example.wicket.LoginWebPage;
import com.example.wicket.LogoutWebPage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestLogoutWebPage extends TestConfiguration {

    @Before
    public void init() {
        wicketTester.startPage(LogoutWebPage.class);
    }

    @Test
    public void testLogoutWebPageRendersSuccessfulToLoginWebPage() {
        wicketTester.assertRenderedPage(LoginWebPage.class);
    }

    @Test
    public void testLogoutWebPageSignsOut() {
        Assert.assertFalse(SecurityConfiguration.get().isSignedIn());
    }

}
