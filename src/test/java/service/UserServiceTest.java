package service;

import com.example.domain.Category;
import com.example.domain.User;
import com.example.exceptions.UserAlreadyExistingException;
import com.example.exceptions.UserNotFoundException;
import com.example.persistence.CategoryRepository;
import com.example.persistence.UserRepository;
import com.example.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.mindrot.jbcrypt.BCrypt;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    private User user;

    private Category category;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        user = new User("username", "password");
        category = new Category("Test Category 1");
    }

    @Test(expected = UserAlreadyExistingException.class)
    public void testSaveUserAlreadyExisting() throws UserAlreadyExistingException {
        when(userRepository.findByUsername(user.getUsername())).thenReturn(user);

        userService.save(user.getUsername(), user.getPasswordHash());
    }

    @Test
    public void testSaveSuccess() throws UserAlreadyExistingException {
        when(userRepository.save(user)).thenReturn(user);
        when(categoryRepository.save(category)).thenReturn(category);

        assertThat(user, is(userRepository.save(user)));
        userService.save(user.getUsername(), user.getPasswordHash());
        verify(userRepository).findByUsername(user.getUsername());
        verify(userRepository).save(user);
    }

    @Test
    public void testAuthenticateFailedWrongPassword() throws UserNotFoundException {
        when(userRepository.findByUsername(user.getUsername())).thenReturn(user);
        when(userRepository.findPasswordHashByUsername(user.getUsername()))
                .thenReturn(BCrypt.hashpw("password", BCrypt.gensalt()));

        assertFalse(userService.authenticate(user.getUsername(), "wrongPassword"));
        verify(userRepository).findPasswordHashByUsername(user.getUsername());
    }

    @Test(expected = UserNotFoundException.class)
    public void testAuthenticateFailedUserDoesNotExist() throws UserNotFoundException {
        when(userRepository.findByUsername("notExistingUser")).thenReturn(null);

        userService.authenticate(user.getUsername(), "wrongPassword");
        verify(userRepository).findByUsername("notExistingUser");
    }

    @Test
    public void testAuthenticateSuccess() throws UserNotFoundException {
        when(userRepository.findByUsername(user.getUsername())).thenReturn(user);
        when(userRepository.findPasswordHashByUsername(user.getUsername()))
                .thenReturn(BCrypt.hashpw("password", BCrypt.gensalt()));

        assertTrue(userService.authenticate(user.getUsername(), "password"));
        verify(userRepository).findPasswordHashByUsername(user.getUsername());
    }

    @Test
    public void testFindByUsername() {
        when(userRepository.findByUsername(user.getUsername())).thenReturn(user);

        assertThat(userService.findByUsername(user.getUsername()), is(user));
        verify(userRepository).findByUsername(user.getUsername());
    }

}
