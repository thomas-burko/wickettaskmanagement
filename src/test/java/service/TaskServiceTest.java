package service;

import com.example.domain.Category;
import com.example.domain.Priority;
import com.example.domain.Task;
import com.example.domain.User;
import com.example.persistence.CategoryRepository;
import com.example.persistence.TaskRepository;
import com.example.persistence.UserRepository;
import com.example.service.TaskService;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TaskServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private CategoryRepository categoryRepository;

    @InjectMocks
    private TaskService taskService;

    private User user;

    private Category category1;

    private Category category2;

    private Task taskWithoutCategory;

    private Task taskUnfinished;

    private Task taskFinished;

    private Task taskFinishedAndDeleted;

    private List<Task> allTasks;

    private List<Task> deletedTasks;

    private List<Task> notDeletedTasks;

    private List<Task> finishedTasks;

    private List<Category> categories;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        category1 = new Category("Test Category 1");
        category2 = new Category("Test Category 2");
        user = new User("username", "password");
        taskUnfinished = new Task(category1, "Test 1", Priority.Minor.toString());
        taskFinished = new Task(category2, "Test 2", Priority.Important.toString());
        taskFinishedAndDeleted = new Task(category1, "Test 3", Priority.Prime.toString());
        taskWithoutCategory =  new Task(category1, "Test 4", Priority.Minor.toString());

        categories = new ArrayList();
        allTasks = new ArrayList();
        deletedTasks = new ArrayList();
        notDeletedTasks = new ArrayList();
        finishedTasks = new ArrayList();

        taskFinished.setFinished(true);
        taskFinishedAndDeleted.setFinished(true);
        taskFinishedAndDeleted.setDeleted(true);
        category1.setUser(user);
        category2.setUser(user);

        categories.add(category1);
        categories.add(category2);
        allTasks.add(taskUnfinished);
        allTasks.add(taskFinishedAndDeleted);
        allTasks.add(taskFinished);
        deletedTasks.add(taskFinishedAndDeleted);
        notDeletedTasks.add(taskFinished);
        notDeletedTasks.add(taskUnfinished);
        finishedTasks.add(taskFinished);

        user.addCategory(category1);
        user.addCategory(category2);
        category1.addTask(taskUnfinished);
        category1.addTask(taskFinishedAndDeleted);
        category2.addTask(taskFinished);
    }

    @Test
    public void testFindOne() {
        when(taskRepository.findOne(taskUnfinished.getId())).thenReturn(taskUnfinished);

        taskService.findOne(taskUnfinished);
        verify(taskRepository).findOne(taskUnfinished.getId());
        assertThat(taskService.findOne(taskUnfinished), Is.is(taskUnfinished));
    }

    @Test
    public void testSaveTask() {
        when(taskRepository.save(taskUnfinished)).thenReturn(taskUnfinished);

        taskService.save(taskUnfinished);
        verify(taskRepository).save(taskUnfinished);
        assertThat(taskService.save(taskUnfinished), Is.is(taskUnfinished));
    }

    @Test
    public void testChangeCheckedToTrue() {
        when(taskRepository.findOne(taskUnfinished.getId())).thenReturn(taskUnfinished);

        assertFalse(taskUnfinished.isFinished());
        taskService.changeChecked(taskUnfinished);
        verify(taskRepository).findOne(taskUnfinished.getId());
        verify(taskRepository).save(taskUnfinished);
        assertTrue(taskUnfinished.isFinished());
    }

    @Test
    public void testChangeCheckedToFalse() {
        when(taskRepository.findOne(taskFinished.getId())).thenReturn(taskFinished);

        assertTrue(taskFinished.isFinished());
        taskService.changeChecked(taskFinished);
        verify(taskRepository).findOne(taskFinished.getId());
        verify(taskRepository).save(taskFinished);
        assertFalse(taskFinished.isFinished());
    }

    @Test
    public void testSoftDeleteTasks() {
        when(taskRepository.findByFinished(true)).thenReturn(finishedTasks);

        taskService.softDelete();
        verify(taskRepository).findByFinished(true);
        verify(taskRepository).save(taskFinished);
        assertTrue(taskFinished.isDeleted());
    }

    @Test
    public void testFindAllNotDeletedTasksByReference() {
        when(userRepository.findByReference(user.getReference())).thenReturn(user);
        when(categoryRepository.findByUser(user)).thenReturn(categories);
        when(taskRepository.findByCategoryAndDeleted(category1, false)).thenReturn(notDeletedTasks);

        assertThat(taskService.findAllNotDeletedTasksByReference(user.getReference()), hasSize(2));
        verify(userRepository).findByReference(user.getReference());
        verify(categoryRepository).findByUser(user);
        verify(taskRepository).findByCategoryAndDeleted(category1, false);
    }

    @Test
    public void testAddTaskToCategory() {
        when(categoryRepository.findOne(category1.getId())).thenReturn(category1);

        taskService.addTaskToCategory(taskWithoutCategory, category1);
        assertThat(taskWithoutCategory.getCategory(), is(category1));
        verify(categoryRepository).findOne(category1.getId());
    }
}