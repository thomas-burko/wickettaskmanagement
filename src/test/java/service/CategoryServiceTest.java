package service;

import com.example.domain.Category;
import com.example.domain.Task;
import com.example.domain.User;
import com.example.persistence.CategoryRepository;
import com.example.persistence.UserRepository;
import com.example.service.CategoryService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CategoryServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private CategoryRepository categoryRepository;

    @InjectMocks
    private CategoryService categoryService;

    private User user;

    private Task task;

    private Category category;

    private Category categoryWithTask;

    private List<Category> categories;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        user = new User("username", "password");
        category = new Category("Test");
        categoryWithTask = new Category("Test 2");
        task = new Task();
        categories = new ArrayList();

        category.setUser(user);
        categoryWithTask.setUser(user);
        categoryWithTask.addTask(task);

        categories.add(category);
        categories.add(categoryWithTask);
    }

    @Test
    public void testSave() {
        when(userRepository.findByReference(user.getReference())).thenReturn(user);
        when(userRepository.save(user)).thenReturn(user);

        categoryService.save(category, user.getReference());
        verify(userRepository).findByReference(user.getReference());
        verify(userRepository).save(user);
        assertThat(user.getCategories(), hasSize(1));
        assertThat(category.getUser(), is(user));
    }

    @Test
    public void testFindCategoriesByUserReference() {
        when(userRepository.findByReference(user.getReference())).thenReturn(user);
        when(categoryRepository.findByUser(user)).thenReturn(categories);

        assertThat(categoryService.findCategoriesByUserReference(user.getReference()), hasSize(2));
        verify(userRepository).findByReference(user.getReference());
        verify(categoryRepository).findByUser(user);
    }
}
