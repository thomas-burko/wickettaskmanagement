package com.example;

import com.example.configuration.SecurityConfiguration;
import com.example.wicket.*;
import org.apache.wicket.Page;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Component
public class WicketApplication extends AuthenticatedWebApplication implements ApplicationContextAware {

    private ApplicationContext ctx;

    @Override
    protected void init() {
        super.init();
        mountPage("/login", LoginWebPage.class);
        mountPage("/task", TaskWebPage.class);
        mountPage("/tasks", TaskListWebPage.class);
        mountPage("/logout", LogoutWebPage.class);
        mountPage("/register", RegisterWebPage.class);
        mountPage("/category", CategoryWebPage.class);
        initApplicationContext();
    }

    private void initApplicationContext() {
        if (ctx == null) {
            ctx = WebApplicationContextUtils.findWebApplicationContext(this.getServletContext());
        }
        getComponentInstantiationListeners().add(new SpringComponentInjector(this, ctx, true));
    }

    @Override
    public Class<? extends Page> getHomePage() {
        return TaskWebPage.class;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }

    @Override
    public Class<? extends WebPage> getSignInPageClass() {
        return LoginWebPage.class;
    }

    @Override
    public Class<? extends AbstractAuthenticatedWebSession> getWebSessionClass() {
        return SecurityConfiguration.class;
    }
}