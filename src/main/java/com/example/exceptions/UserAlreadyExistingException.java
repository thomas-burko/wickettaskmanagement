package com.example.exceptions;

public class UserAlreadyExistingException extends Exception {

    public UserAlreadyExistingException() {
        super("User is already existing in database!");
    }
}
