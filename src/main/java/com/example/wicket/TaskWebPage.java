package com.example.wicket;

import com.example.configuration.SecurityConfiguration;
import com.example.wicket.form.TaskForm;
import com.example.wicket.navigation.TaskNavigation;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;

@AuthorizeInstantiation("USER")
public class TaskWebPage extends WebPage {

    public TaskWebPage() {

    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new TaskNavigation("navigation"));
        add(new TaskForm("taskForm"));
        add(new Label("currentUser", SecurityConfiguration.getUser().getUsername()));
    }
}
