package com.example.wicket.form;

import com.example.configuration.SecurityConfiguration;
import com.example.domain.Priority;
import com.example.domain.Task;
import com.example.service.CategoryService;
import com.example.service.TaskService;
import com.example.wicket.TaskListWebPage;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.validation.validator.StringValidator;

import java.util.Arrays;

public class TaskForm extends Form<Task> {

    @SpringBean
    private TaskService taskService;

    @SpringBean
    private CategoryService categoryService;

    public TaskForm(String id) {
        super(id, new CompoundPropertyModel(new Task()));
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new DropDownChoice("category", categoryService.findCategoriesByUserReference(SecurityConfiguration.getUser().getReference()),
                new ChoiceRenderer<>("categoryName")).setRequired(true));
        add(new TextField<String>("text").add(StringValidator.minimumLength(3)));
        add(new DropDownChoice("priority", Arrays.asList(Priority.values())).setRequired(true));
        add(new DateTextField("date").setRequired(true));
    }

    @Override
    public final void onSubmit() {
        Task task = new Task();
        task.setText(getModelObject().getText());
        task.setPriority(getModelObject().getPriority());
        task.setDate(getModelObject().getDate());
        taskService.addTaskToCategory(task, getModelObject().getCategory());
        setResponsePage(new TaskListWebPage());
    }
}