package com.example.wicket.form;

import com.example.domain.Task;
import com.example.service.TaskService;
import com.example.wicket.TaskListWebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class RemoveTaskForm extends Form<Task> {

    @SpringBean
    public TaskService taskService;

    public RemoveTaskForm(String id) {
        super(id);
    }

    @Override
    public final void onSubmit() {
        taskService.softDelete();
        setResponsePage(new TaskListWebPage());
    }

}
