package com.example.wicket.form;

import com.example.configuration.SecurityConfiguration;
import com.example.wicket.TaskWebPage;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.util.value.ValueMap;
import org.apache.wicket.validation.validator.StringValidator;

public class LoginForm extends StatelessForm<ValueMap> {

    public LoginForm(String id) {
        super(id, new CompoundPropertyModel(new ValueMap()));
        redirectIfSignedIn();
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new TextField<String>("username").add(StringValidator.minimumLength(3)));
        add(new PasswordTextField("password").add(StringValidator.minimumLength(3)));
    }

    @Override
    protected void onSubmit() {
        ValueMap values = getModelObject();
        SecurityConfiguration.get().signIn(values.get("username").toString(), values.get("password").toString());
        redirectIfSignedIn();
    }

    private void redirectIfSignedIn() {
        if (SecurityConfiguration.get().isSignedIn()) {
            setResponsePage(new TaskWebPage());
        }
    }

}
