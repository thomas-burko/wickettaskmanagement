package com.example.wicket.form;

import com.example.configuration.SecurityConfiguration;
import com.example.domain.Category;
import com.example.service.CategoryService;
import com.example.wicket.CategoryWebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class CategoryForm extends Form<Category> {

    @SpringBean
    private CategoryService categoryService;

    public CategoryForm(String id) {
        super(id, new CompoundPropertyModel(new Category()));
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new TextField<String>("categoryName").setRequired(true));
    }

    @Override
    protected void onSubmit() {
        Category category = getModelObject();
        categoryService.save(category, SecurityConfiguration.getUser().getReference());
        setResponsePage(CategoryWebPage.class);
    }

}
