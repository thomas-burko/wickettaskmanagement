package com.example.wicket.form;

import com.example.configuration.SecurityConfiguration;
import com.example.exceptions.UserAlreadyExistingException;
import com.example.service.UserService;
import com.example.wicket.TaskWebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.value.ValueMap;
import org.apache.wicket.validation.validator.StringValidator;

public class RegisterForm extends Form<ValueMap> {

    @SpringBean
    private UserService userService;

    public RegisterForm(String id) {
        super(id, new CompoundPropertyModel(new ValueMap()));
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new TextField<String>("username").add(StringValidator.minimumLength(3)));
        add(new PasswordTextField("password").add(StringValidator.minimumLength(3)));
    }

    @Override
    public final void onSubmit() {
        ValueMap values = getModelObject();
        try {
            userService.save(values.get("username").toString(), values.get("password").toString());
            SecurityConfiguration.get().signIn(values.get("username").toString(), values.get("password").toString());
        } catch (UserAlreadyExistingException e) {
            System.out.println(e);
        }
        setResponsePage(TaskWebPage.class);
    }

}
