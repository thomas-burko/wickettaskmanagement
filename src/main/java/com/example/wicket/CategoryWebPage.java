package com.example.wicket;

import com.example.wicket.form.CategoryForm;
import com.example.wicket.listView.CategoryListView;
import com.example.wicket.navigation.TaskNavigation;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.WebPage;

@AuthorizeInstantiation("USER")
public class CategoryWebPage extends WebPage {

    public CategoryWebPage() {

    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new TaskNavigation("navigation"));
        add(new CategoryForm("categoryForm"));
        add(new CategoryListView("categories"));
    }
}
