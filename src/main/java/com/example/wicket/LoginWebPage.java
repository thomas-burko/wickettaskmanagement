package com.example.wicket;

import com.example.wicket.form.LoginForm;
import com.example.wicket.navigation.TaskNavigation;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.panel.FeedbackPanel;

public class LoginWebPage extends WebPage {

    public LoginWebPage() {

    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new TaskNavigation("navigation"));
        add(new LoginForm("loginForm"));
        add(new FeedbackPanel("feedbackPanelLogin"));
    }
}
