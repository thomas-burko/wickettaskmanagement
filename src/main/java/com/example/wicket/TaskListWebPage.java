package com.example.wicket;

import com.example.wicket.form.RemoveTaskForm;
import com.example.wicket.listView.TaskListView;
import com.example.wicket.navigation.TaskNavigation;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.WebPage;

@AuthorizeInstantiation("USER")
public class TaskListWebPage extends WebPage {

    public TaskListWebPage() {

    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new TaskNavigation("navigation"));
        add(new TaskListView("tasks"));
        add(new RemoveTaskForm("removeTask"));
    }
}
