package com.example.wicket;

import com.example.wicket.form.RegisterForm;
import com.example.wicket.navigation.TaskNavigation;
import org.apache.wicket.markup.html.WebPage;

public class RegisterWebPage extends WebPage {

    public RegisterWebPage() {

    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new TaskNavigation("navigation"));
        add(new RegisterForm("registerForm"));
    }
}
