package com.example.wicket.listView;

import com.example.configuration.SecurityConfiguration;
import com.example.domain.Task;
import com.example.service.TaskService;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxCheckBox;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import java.util.Date;

public class TaskListView extends ListView<Task> {

    @SpringBean
    private TaskService taskService;

    public TaskListView(String id) {
        super(id);
        setList(taskService.findAllNotDeletedTasksByReference(SecurityConfiguration.getUser().getReference()));
    }

    @Override
    protected void populateItem(final ListItem<Task> item) {
        item.add(new Label("author", new PropertyModel<String>(item.getDefaultModel(), "category.categoryName")));
        item.add(new Label("text", new PropertyModel<String>(item.getDefaultModel(), "text")));
        item.add(new Label("date", new PropertyModel<Date>(item.getDefaultModel(), "date")));
        item.add(new Label("priority", new PropertyModel<String>(item.getDefaultModel(), "priority")));
        item.add(new AjaxCheckBox("finish", new PropertyModel<Boolean>(item.getDefaultModel(), "finished")) {
            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                taskService.changeChecked(item.getModelObject());
            }
        });
    }
}
