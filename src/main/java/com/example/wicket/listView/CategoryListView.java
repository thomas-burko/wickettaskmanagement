package com.example.wicket.listView;

import com.example.configuration.SecurityConfiguration;
import com.example.service.CategoryService;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class CategoryListView extends ListView {

    @SpringBean
    private CategoryService categoryService;

    public CategoryListView(String id) {
        super(id);
        setList(categoryService.findCategoriesByUserReference(SecurityConfiguration.getUser().getReference()));
    }

    @Override
    protected void populateItem(ListItem item) {
        item.add(new Label("categoryName", new PropertyModel<String>(item.getDefaultModel(), "categoryName")));
    }
}
