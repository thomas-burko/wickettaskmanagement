package com.example.wicket;

import com.example.configuration.SecurityConfiguration;
import org.apache.wicket.markup.html.WebPage;

public class LogoutWebPage extends WebPage {

    public LogoutWebPage() {

    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        SecurityConfiguration.get().signOut();
        setResponsePage(LoginWebPage.class);
    }
}
