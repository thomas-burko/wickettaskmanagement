package com.example.service;

import com.example.domain.Category;
import com.example.domain.Task;
import com.example.persistence.CategoryRepository;
import com.example.persistence.TaskRepository;
import com.example.persistence.UserRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class TaskService {

    private static final Logger logger = Logger.getLogger(TaskService.class);

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private UserRepository userRepository;

    public Task findOne(Task task) {
        logger.info("Calling TaskService: findOne(" + task + ")");
        return taskRepository.findOne(task.getId());
    }

    public Task save(Task task) {
        logger.info("Calling TaskService: save(" + task + ")");
        return taskRepository.save(task);
    }

    public void changeChecked(Task task) {
        logger.info("Calling TaskService: changeChecked(" + task + ")");
        Task updatedTask = taskRepository.findOne(task.getId());
        logger.info("Found a task: " + task);
        updatedTask.setFinished(!updatedTask.isFinished());
        logger.info("Setting finished from " + !updatedTask.isFinished() + " to " + updatedTask.isFinished());
        taskRepository.save(updatedTask);
    }

    public void softDelete() {
        logger.info("Calling TaskService: softDelete()");
        List<Task> tasks = taskRepository.findByFinished(true);
        logger.info("Found " + tasks.size() + " to delete");
        for (Task task : tasks) {
            task.setDeleted(true);
            taskRepository.save(task);
        }
    }

    public List<Task> findAllNotDeletedTasksByReference(UUID reference) {
        logger.info("Calling TaskService: findAllNotDeletedTasksByReference( " + reference + ")");
        List<Category> categories = categoryRepository.findByUser(userRepository.findByReference(reference));
        logger.info("Found " + categories.size() + " Categories for reference " + reference);
        List<Task> tasks = new ArrayList();
        for (Category c : categories) {
            tasks.addAll(taskRepository.findByCategoryAndDeleted(c, false));
        }
        return tasks;
    }

    public void addTaskToCategory(Task task, Category category) {
        logger.info("Calling TaskService: addTaskToCategory( " + task + ", + " + category + ")");
        Category updatedCategory = categoryRepository.findOne(category.getId());
        logger.info("Found a category: " + updatedCategory);
        task.setCategory(category);
        taskRepository.save(task);
        logger.info("Setting Category for task and save it in the database: " + task);
        updatedCategory.addTask(task);
        logger.info("Add " + task + "to Category: ");
        categoryRepository.save(updatedCategory);
    }

}