package com.example.service;

import com.example.domain.Category;
import com.example.domain.User;
import com.example.exceptions.UserAlreadyExistingException;
import com.example.exceptions.UserNotFoundException;
import com.example.persistence.CategoryRepository;
import com.example.persistence.UserRepository;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    public User save(String username, String password) throws UserAlreadyExistingException {
        if (userRepository.findByUsername(username) == null) {
            Category category = new Category("General");
            User u = userRepository.save(new User(username, BCrypt.hashpw(password, BCrypt.gensalt())));
            category.setUser(u);
            categoryRepository.save(category);
            return u;
        }
        throw new UserAlreadyExistingException();
    }

    public boolean authenticate(String username, String password) throws UserNotFoundException {
        if (userRepository.findByUsername(username) != null) {
            return BCrypt.checkpw(password, (userRepository.findPasswordHashByUsername(username)));
        }
        throw new UserNotFoundException();
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

}
