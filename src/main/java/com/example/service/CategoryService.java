package com.example.service;

import com.example.domain.Category;
import com.example.domain.User;
import com.example.persistence.CategoryRepository;
import com.example.persistence.UserRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class CategoryService {

    private static final Logger logger = Logger.getLogger(CategoryService.class);

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private UserRepository userRepository;

    public Category save(Category category, UUID reference) {
        logger.info("Calling CategoryService: save(" + category + ", " + reference + ")");
        User u = userRepository.findByReference(reference);
        logger.info("Found a user with the reference '" + reference + "': " + u);
        u.addCategory(category);
        category.setUser(u);
        userRepository.save(u);
        logger.info("Saved '" + u + "' in database");
        return category;
    }

    public List<Category> findCategoriesByUserReference(UUID reference) {
        logger.info("Calling CategoryService: findCategoriesByUserReference(" + reference + ")");
        return categoryRepository.findByUser(userRepository.findByReference(reference));
    }
}
