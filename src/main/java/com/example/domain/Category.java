package com.example.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
public class Category extends BaseEntity {

    @Getter
    private String categoryName;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Getter
    private List<Task> tasks;

    @ManyToOne
    @Getter
    @Setter
    private User user;

    public Category(String categoryName) {
        this.categoryName = categoryName;
        this.tasks = new ArrayList();
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

}
