package com.example.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
public class User extends BaseEntity {

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private String passwordHash;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Getter
    private List<Category> categories;

    public User(String username, String passwordHash) {
        this.username = username;
        this.passwordHash = passwordHash;
        categories = new ArrayList();
    }

    public void addCategory(Category category) {
        categories.add(category);
    }

}
