package com.example.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
@NoArgsConstructor
public class Task extends BaseEntity {

    @ManyToOne
    @Getter
    @Setter
    private Category category;

    @Getter
    @Setter
    private String text;

    @Getter
    @Setter
    private String priority;

    @Getter
    @Setter
    private Date date;

    @Getter
    @Setter
    private boolean finished;

    @Getter
    @Setter
    private boolean deleted;

    public Task(Category category, String text, String priority) {
        this.category = category;
        this.text = text;
        this.priority = priority;
    }
}
