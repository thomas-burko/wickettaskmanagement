package com.example.domain;

public enum Priority {

    Minor, Important, Prime

}
