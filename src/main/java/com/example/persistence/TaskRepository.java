package com.example.persistence;

import com.example.domain.Category;
import com.example.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    List<Task> findByFinished(boolean finished);

    List<Task> findByCategoryAndDeleted(Category category, boolean deleted);

}
