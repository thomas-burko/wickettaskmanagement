package com.example.persistence;

import com.example.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByReference(UUID reference);

    User findByUsername(String username);

    @Query("select passwordHash from User u where u.username = ?1")
    String findPasswordHashByUsername(String username);

}
