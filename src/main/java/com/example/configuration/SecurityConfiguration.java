package com.example.configuration;

import com.example.domain.User;
import com.example.exceptions.UserNotFoundException;
import com.example.service.UserService;
import org.apache.log4j.Logger;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.request.Request;
import org.apache.wicket.spring.injection.annot.SpringBean;

public class SecurityConfiguration extends AuthenticatedWebSession {

    private static Logger logger = Logger.getLogger(SecurityConfiguration.class);

    @SpringBean
    private UserService userService;

    private static User user = null;

    public SecurityConfiguration(Request request) {
        super(request);
        Injector.get().inject(this);
    }

    @Override
    public boolean authenticate(final String username, final String password) {
        try {
            if (userService.authenticate(username, password)) {
                user = userService.findByUsername(username);
                return true;
            }
        } catch (UserNotFoundException e) {
            logger.error(e);
        }
        return false;
    }

    @Override
    public Roles getRoles() {
        if (isSignedIn()) {
            return new Roles(Roles.USER);
        }
        return null;
    }

    public static User getUser() {
        return user;
    }
}
